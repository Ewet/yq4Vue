import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex);
import { SOME_MUTATION } from './mutation-types';
import state from './state';
import mutations from './mutations';
import actions from './actions';
import { iview1_1,iview1_2 } from './modules';
const store = new Vuex.Store({
    state,
    mutations,
    actions,
    modules:{
        a:iview1_1,
        b:iview1_2,
    },
})
export default store;








