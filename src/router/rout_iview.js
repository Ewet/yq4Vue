import iview from '../components/iview';
import iview1 from '../module/iview/1-1';
import iview2 from '../module/iview/1-2';

export default {
    path: '/iview',
    name: 'iview', //history模式的配置；
    meta: {
        titile: 'iview',//标题
        rout: 'iview' //判断是否显示脚部；
    },
    component: iview,
        children: [
            {
                path: '/iview/1-1',
                name: 'iview1', //history模式的配置；
                meta: {
                    titile: 'iview1-1',//标题
                    rout: '1-1' //判断是否显示脚部；
                },
                components: {
                    iview: iview1
                }
            },
            {
                path: '/iview/1-2',
                name: 'iview2', //history模式的配置；
                meta: {
                    titile: 'iview1-2',//标题
                    rout: '1-2' //判断是否显示脚部；
                },
                components: {
                    iview: iview2
                }
            },
        ]
}

























