import home from '../components/home';
import brand from '../components/brand';
import shopCar from '../components/shopCar';
import person from '../components/person';
import pro_list from '../components/pro_list';

export default [
    {
        path: '/',
        redirect: '/index' //路由重定向；
    },
    {
        path: '/index',
        name: 'index',
        meta: {
            title: '首页',
            rout: 'index'
        },
        component: home
    },
    {
        path: '/brand',
        name: 'brand',
        meta: {
            title: '分类',
            rout: 'brand'
        },
        component: brand
    },
    {
        path: '/shopCar',
        name: 'shopCar',
        meta: {
            title: '购物车',
            rout: 'shopCar'
        },
        component: shopCar
    },
    {
        path: '/person',
        name: 'person',
        meta: {
            title: '我的',
            rout: 'person'
        },
        component: person
    },
    {
        path: '/pro_list',
        name: 'pro_list',
        meta: {
            titile: '商品列表',
            rout: 'pro_list'
        },
        component: pro_list
    },
]