import Vue from 'vue'
import Router from 'vue-router'
//页面组件

//引入外部js使用./做起始路径；
import rout_home from './rout_home';
import rout_iview from './rout_iview';
import test from './test';
// 使用路由
Vue.use(Router)
//动态改变标题实例化参数
Vue.use(require('vue-wechat-title')); 

export default new Router({
  mode:"hash",
  routes: [
    ...rout_home,
    rout_iview,
    ...test
    
  ]
})

// this.$route.matched[1].path  // 获取到地址拦上#号后面的url地址
// this.$parent.footer = false;
// this.active = this.$route.meta.rout;
