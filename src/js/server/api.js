
const testUrl = "http://127.0.0.1:8080"

import axios from 'axios';

const apiUrl = {
    
    iviewUrl: '/static/data/iview.json',
    
}

const aipAjax = {
    _get: function(url,callback) {
        axios.get(url, callback)
        .then((res) => {
            callback(res)
        })
        .catch((response) => {
            // alert("数据请求失败")
            alert(response)
        })
    }
}

export default{
    apiUrl,
    aipAjax,
}










