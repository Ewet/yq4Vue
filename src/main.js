// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store/store';

//mint-ui
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(MintUI)
//使用vue-awesome-swiper （需要通过npm全局安装之后才可以用）
import 'swiper/dist/css/swiper.min.css';
import VueAwesomeSwiper from 'vue-awesome-swiper';
Vue.use(VueAwesomeSwiper) //记得不要忘记这句
//图片懒加载
import VueLazyLoad from 'vue-lazyload';
Vue.use(VueLazyLoad
//	,
//	{
//  error:'../../../static/images/mrmrmr.png',//加载错误时的显示图片
//  loading:'../../../static/images/loading.gif'//加载过程中的过渡图片
//}
  )
//iview
import iView from 'iview';
import 'iview/dist/styles/iview.css'; 
Vue.use(iView);
//xiaos
// import axios from 'axios'
// import VueAxios from 'vue-axios'
// Vue.use(VueAxios, axios)
// Vue.prototype.$axios = axios;  //有可能不支持use方法；

//vuex
// import Vuex from "vuex"
// Vue.use(Vuex)

//api
import global from '@/js/server/api.js';
Vue.prototype.$global = global.apiUrl;
Vue.prototype.$ajax = global.aipAjax;

//外部文件
import '@/js/font_size.js';
import '@/css/common.css';
import '@/css/iconfont/iconfont.css';
Vue.config.productionTip = false

// 创建和挂载根实例
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})




