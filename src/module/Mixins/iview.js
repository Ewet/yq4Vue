import { mapState,mapMutations } from 'vuex';
export default {
  methods:{
    ...mapMutations({

    })
  },
  mounted(){
    this.$store.commit("b/changename","ting")
  },
  computed: {
    ...mapState({
      num: (state) => state.b.num,
      name: (state) => state.b.name,
      age: (state) => state.b.age,
      sex: (state) => state.b.sex,
    })
  }
}
